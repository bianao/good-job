import { RouteRecordRaw, createRouter, createWebHistory } from 'vue-router';
const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('../views/home/index.vue'),
        meta: { requiresAuth: true },
        children: [
            {
                path: '/home',
                name: 'home',
                component: () => import('../views/home/homePage/index.vue'),
            },
            {
                path: '/prodinfo',
                name: 'prodinfo',
                component: () => import('../views/home/product/produce/prodInfo.vue')
            },
            {
                path: `/prodinfoe/:id`,
                name: 'prodinfoe',
                component: () => import('../views/home/product/produce/prodInfoedit.vue')
            },
        ]
    },

    {
        path: '/login',
        name: 'login',
        component: () => import('../views/login/index.vue')
    },
    {
        path: '/:pathMatch(.*)',
        component: () => import('../views/404/index.vue')
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

// 路由守卫
router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token')
    if (to.meta.requiresAuth && !token) {
        next({
            name: 'login'
        })
    } else {
        next()
    }
})


export default router