module.exports = {
  env: {
    root: true, //当前的配置根配置，不要再从她的上级目录去查找文件
    browser: true,
    es2021: true,
    'vue/setup-compiler-macros':true
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-essential',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'standard',
  ],
  overrides: [],
  parser:'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
    parser:'@typescript-eslint/parser',
    sourceType: 'module'
  },
  plugins: [
    'vue',
    '@typescript-eslints'
  ],
  rules: {
    semi:'off',
    'comma-dangle':'off',
    'vue/multi-word-component-names':'off',
    '@typescript-eslint/no-var-requires':'off'
  }
}

//eslint-config-prettier 解决eslint中样式规范和prettier中的样式规范冲突时 以prettier为准
//eslint-plugin-prettier 将prettier作为eslint的规范来用