import request from '../utils/index'
import { PROUPLIST, RequesGroup } from '../types/product/group.d'

export const get_groupList = (params: any) => { //分组管理
    return request.get<RequesGroup<PROUPLIST>>({
        url: '/api/prod/prodTag/page',
        method: 'GET',
        params: params
    })
}
export const del_groupList = (id: number) => { //删除
    return request.delete({
        url: `/api/prod/prodTag/${id}`
    })
}
export const add_groupList = (data:any) => { //新增
    return request.post({
        url: `/api/prod/prodTag`,
        data: data
    })
}
export const edit_groupList = (data:any) => { //编辑
    return request.put({
        url: `/api/prod/prodTag`,
        data: data
    })
}