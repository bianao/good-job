import { defineStore } from 'pinia'
import { _login, get_userinfo, get_menulist } from '../../api/login'
import local from "../../utils/util"
import { getRoutes } from '../../utils/permission'
import router from '../../router'

const login = defineStore('login', {
    state() {
        return {
            token: local.getCache('token') || '',
            authorities: local.getCache('authorities') || [],
            userInfo: local.getCache('userInfo') || {},
            menuList: local.getCache('menuList') || [],
        }
    },
    actions: {
        async loginAction(payload: any) {
            const { data } = await _login(payload)
            // 存储token
            local.setCache('token', `${data.token_type}${data.access_token}`)
            local.setCache('authorities', data.authorities)
            this.token = `${data.token_type}${data.access_token}`

            // 获取用户信息
            const userInfo = await get_userinfo()
            this.userInfo = userInfo.data
            local.setCache('userinfo', userInfo.data)
            router.push('/home')
            // 获取导航列表
            const menuList = await get_menulist()
            this.menuList = menuList.data.menuList
            local.setCache('menuList', menuList.data.menuList)
            this.changeMenu(menuList.data.menuList)
        },
        changeMenu(payload: any) {
            const routes = getRoutes(payload);
            // 动态添加到路由表里面
            // addRoutes addRoute
            // addRoute 路由种的api 是增加一级路由 如果要增加到二级路由一定要带上父路由地址
            routes.forEach(item => {
                router.addRoute('home', item);
            })
        },
        getMenu(){
            // 防止刷新的时候丢失menu丢失数据
            // 刷新的时候 重新获取menu 从本地
            const menuList = [...this.menuList]
            const authorities = [...this.authorities]
            if(menuList){
                this.menuList = [...menuList]
                this.authorities = [...authorities]
                //动态添加路由
                this.changeMenu([...menuList])
            }
        }
    }
})

export default login