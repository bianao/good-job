export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  searchMenuSpan:4,
  column: [
    {
      label: '标签名称',
      prop: 'title',
      search: true
    },
    {
      label: '状态',
      prop: 'status',
      type: 'select',
      slot: true,
      search: true,
      dicData: [
        {
          label: '禁用',
          value: 0
        }, {
          label: '正常',
          value: 1
        }
      ]
    },
    {
      label: '默认类型',
      prop: 'isDefault',
      slot: true,
      dicData: [
        {
          label: '自定义类型',
          value: 0
        }, {
          label: '默认类型',
          value: 1
        }
      ]
    },
    {
      label: '排序',
      prop: 'seq'
    }
  ]
} as any