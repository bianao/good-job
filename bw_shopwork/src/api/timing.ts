import request from '../utils'
interface addrtype {
    data: any;
    current: number;
    size: number;
  }
export const gettimer = (params:any) =>
  request.get<addrtype>({
    url:`/api/sys/schedule/page`,
    method: 'GET',
    params:params
  })

//   删除
export const timerdel=(data:any)=>{
    return request.delete({
        url:`/api/sys/schedule`,
        method: 'delete',
        data
    })
}


// 新增
export const timeadd=(data:any)=>{
    return request.post({
        url:`/api/sys/schedule`,
        method:'post',
       data:data
    })
}
//编辑
export const timeedit=(data:any)=>{
    return request.put({
        url:'/api/sys/schedule',
       data
    })
}
// 暂停
export const timestop=(data:any)=>{
    return request.post({
        url:'/api/sys/schedule/pause',
        method:'post',
        data
    })
}
// 恢复
export const timesopen=(data:any)=>{
    return request.post({
        url:'/api/sys/schedule/resume',
        method:'post',
        data
    })
}
// 立即执行
export const timerun=(data:any)=>{
    return request.post({
        url:`/api/sys/schedule/run`,
        method:'post',
        data
    })
}
// 日志列表
export const timingloge=(params:any)=>
    request.get<addrtype>({
        url:`/api/sys/scheduleLog/page`,
        method: 'GET',
        params:params
    })