export const formatUrl = (url, query) => {
   return Object.keys(query).length ? (url + "?" + Object.keys(query).map(key => `${key}=${query[key]}`).join("&")) : url;
}



// 定义跳路由的方法
export default function ({
   url = '',
   query = {},
   redirect = false,
   tabbar = false
}) {
   url = formatUrl(url, query);

   const typeName = redirect ? "redirectTo" : tabbar ? "switchTo" : "navigateTo";
   return new Promise((resolve, reject) => {
      uni[typeName]({
         url,
         success(res) {
            resolve(res)
         },
         fail(err) {
            reject(err)
         }
      })
   })
}