import request from '../utils'
interface REIGHT{
    chargeType:number;
    createTime:string;
    hasFreeCondition:number;
    isFreeFee:number;
    shopId:number;
    transName:string;
    transfeeFrees:null;
    transfees:null;
    transportId:number;
}
interface HT{
    current:number;
    pages:number;
    searchCount:boolean;
    size:number;
    total:number;
    records:REIGHT[]
}

interface RequesGroup<T>{
    statusText:string
    status:number
    request: T
    headers:T
    data:T
    config:T
}
//获取公告管理数据
export const _getnoticeList=(query?:any)=>{
    return request.get<RequesGroup<HT>>({
        url:`/api/shop/notice/page`,
        params:query
    })
}
//删除
export const _deletenoticeList=(id:number)=>{
   return request.delete({
    url:`/api/shop/notice/${id}`
   })
}
//新增
export const _addnoticeList=(data:any)=>{
    return request.post({
        url:'/api/shop/notice',
        data
    })
}
//编辑
export const _updatenoticeList=(data:any)=>{
    return request.put({
        url:'/api/shop/notice',
        data
    })
}




//获取热搜管理数据
export const _getsearchList=(query?:any)=>{
     return request.get<RequesGroup<HT>>({
        url:'/api/admin/hotSearch/page',
        params:query
     })
}
//热搜管理新增
export const _addsearchList=(data:any)=>{
    return request.post({
        url:'/api/admin/hotSearch',
        data
    })
}
//热搜管理编辑
export const _updatesearchList=(data:any)=>{
    return request.put({
        url:'/api/admin/hotSearch',
        data
    })
}
//热搜管理删除
export const _deletesearchList=(id:number[])=>{
    return request.delete({
     url:`/api/admin/hotSearch`,
     data:id
    })
 }




 export interface Swiper{
    //@ts-ignore
    data: Swiper<Swiperdata>;
    current: number;
    size: number;
}
export interface Swiperdata{
    $cellEdit:boolean,
    $index:number,
    $status:string,
    des:string,
    imgId:number,
    imgUrl:string,
    link:null,
    pic:null,
    prodName:null,
    relation:number,
    seq:number,
    shopId:number,
    status:number,
    title:null,
    type:number,
    uploadTime:string
}
export interface Hotsearch{
    // @ts-ignore
    data: Hotsearch<Hotsearchdata>;
}
export interface Hotsearchdata{
    $cellEdit:boolean,
    $index:number,
    $status:string,
    content:string,
    hotSearchId:number,
    recDate:string,
    seq:number,
    shopId:number,
    status:number,
    title:string
}


//获取轮播图管理数据
export const SwiperList = (params:any) =>
  request.get<Swiper>({
    url:`/api/admin/indexImg/page`,
    method: 'GET',
    params:params
  })
// 删除
export const delswiper=(data:any)=>{
  return request.delete({
    url:`/api/admin/indexImg`,
    method: 'delete',
    data
  })
}

// 新增
export const swiperadd=(data:any)=>{
  return request.post({
    url:`/api/admin/indexImg`,
    method:'post',
    data
  })
}
//编辑
export const swiperedit=(data:any)=>{
  return request.put({
    url:`/api/admin/indexImg`,
    method:'put',
    data
  })
}

// 选择商品
export const swipertypelist=(params:any)=>
  request.get<Swiper>({
    url:`/api/prod/prod/page?t=1667272692789&current=1&size=10`,
    method: 'GET',
    params:params
  })

  // 商品
  export const swipertypecheck=(params:any)=>
    request.get<Swiper>({
      url:`/api/prod/prod/page?t=1667360385790&current=1&size=10`,
      method: 'GET',
      params:params
    })





//获取运费管理数据
export const _getfeightList=(query?:any)=>{
    return request.get<RequesGroup<HT>>({
       url:'/api/shop/transport/page',
       params:query
    })
}
//获取运费管理 弹窗回显数据
export const _getfeList=(id:number)=>{
    return request.get<RequesGroup<any>>({
       url:`/api/shop/transport/info/${id}`,
    })
}
//获取选择配送区域数据
export const _getaddr=()=>{
    return request.get<RequesGroup<any>>({
      url:'/api/admin/area/list'
    })
}
//删除运费管理数据
export const _deletefeightList=(data:any)=>{
    return request.delete({
       url:'/api/shop/transport',
      data
    })
}
//编辑运费管理数据
export const _updatefeightList=(data:any)=>{
    return request.put({
       url:'/api/shop/transport',
       data
    })
}
//新增运费管理数据
export const _addfeightList=(data:any)=>{
    return request.post({
       url:'/api/shop/transport',
       data
    })
}




//获取自提点管理数据 /shop/pickAddr/page
export const _getpicktList=(query?:any)=>{
    return request.get<RequesGroup<HT>>({
       url:'/api/shop/pickAddr/page',
       params:query
    })
}
//获取自提点管理的地市数据
export const _getaddrList=(query:any)=>{
    return request.get<RequesGroup<HT>>({
       url:`/api/admin/area/listByPid`,
       params:query
    })
}
//新增自提点管理
export const _addpicktList=(data:any)=>{
    return request.post<RequesGroup<HT>>({
       url:'/api/shop/pickAddr',
       data
    })
}
//编辑自提点管理
export const _updatepicktList=(data:any)=>{
    return request.put<RequesGroup<HT>>({
       url:'/api/shop/pickAddr',
       data
    })
}
///删除自提点管理
export const _deletepickList=(id:number[])=>{
    return request.delete({
     url:`/api/shop/pickAddr`,
     data:id
    })
 }