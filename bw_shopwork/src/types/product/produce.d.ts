export interface RequesProd {
    status: number,
    statusText: string,
    data: ProdTYPE,
}

export interface ProdTYPE {
    searchCount: boolean,
    size: number,
    total: number,
    current: number,
    pages: number,
    records: ProdLIST
}

export interface ProdLIST {
    $cellEdit: boolean,
    $index: number,
    $status: string,
    brief: string,
    categoryId: number,
    content: string,
    createTime: string,
    deliveryMode: string,
    deliveryTemplateId: number,
    imgs: string,
    oriPrice: number,
    pic: string,
    price: number,
    prodId: number,
    prodName: string,
    putawayTime: string,
    shopId: number,
    shopName: any,
    skuList: any,
    soldNum: any,
    status: number,
    tagList: any,
    totalStocks: number,
    updateTime: string,
    version: any
}