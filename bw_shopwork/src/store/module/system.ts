import { defineStore } from 'pinia'
import { get_userlist, get_rolelist, get_canshulist, get_arealist, get_timelist, get_menutable, get_menuTable, get_menulist } from "../../api/system"
import { treeDataTranslate } from '../../utils/tool'
const systemStore: any = defineStore('system', {
    state() {
        return {
            adminlist: [],
            admintotal: 0,
            roleList: [],
            roletotal: 0,
            canshulist: [],
            canshutotal: 0,
            arealist: [],
            timelist: [],
            soulist: [],
            menuList: [],
            menuTable: [],
            MenuList: [],
            systemLists: [],
            systemtotal: 0
        }
    },
    actions: {
        async getUsertAction(params: any) {
            let res: any = await get_userlist(params)
            this.adminlist = res.data.records
            this.admintotal = res.data.total
        },
        async getrolelistAction(params: any) {
            let res: any = await get_rolelist(params)
            this.roleList = res.data.records
            this.roletotal = res.data.total
        },
        async getmenultableAction() {
            let res: any = await get_menutable()
            this.menuList = treeDataTranslate(res.data, 'menuId', 'parentId')
        },
        async getcanshuAction(params: any) {
            let res: any = await get_canshulist(params)
            this.canshulist = res.data.records
            this.canshutotal = res.data.total
        },
        async getareaAction() {
            let res: any = await get_arealist()
            this.arealist = treeDataTranslate(res.data, 'areaId', 'parentId')
            this.soulist = this.arealist
        },
        async getTimeAction() {
            let res: any = await get_timelist()
            this.timelist = res.data.records
        },
        async souAction(val: string) {
            this.soulist = this.arealist.filter((item: any) =>
                item.areaName.includes(val)
            )
        },
        async getmenuTable() {
            let res: any = await get_menuTable()
            this.menuTable = treeDataTranslate(res.data, 'menuId')
        },
        async getMenuList() {
            let res: any = await get_menulist()
            this.MenuList = treeDataTranslate(res.data, 'menuId')
        },

    }
})
export default systemStore