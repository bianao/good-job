export interface RequesProd {
    status: number,
    statusText: string,
    data: ProdTYPE,
}

export interface ClassTYPE {
    searchCount: boolean,
    size: number,
    total: number,
    current: number,
    pages: number,
    records: ClassLIST[]
}

export interface ClassLIST {
    prodPropValues: ClassLISTItem,
    propId: number,
    propName: string,
    rule: number,
    shopId: any
}

export interface ClassLISTItem {
    propId: any,
    propValue: string,
    valueId: number
}