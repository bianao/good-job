import request from '../utils'
import { RequesProd, ClassTYPE } from '../types/product/prodSpec'

export const _get_spec = (params: any) => { //产品管理
    //@ts-ignore
    return request.get<RequesProd<ClassTYPE>>({
        url: '/api/prod/spec/page',
        method: 'GET',
        params: params
    })
}

export const _add_spec = (params: any) => { //新增产品管理
    //@ts-ignore
    return request.post({
        url: '/api/prod/spec',
        method: 'POST',
        data: params
    })
}
export const _edit_spec = (params: any) => { //编辑产品管理
    //@ts-ignore
    return request.put({
        url: '/api/prod/spec',
        method: 'PUT',
        data: params
    })
}
export const _del_spec = (params: number) => { //删除产品管理
    //@ts-ignore
    return request.delete({
        url: `/api/prod/spec/${params}`,
        method: 'DELETE',
        data: [params]
    })
}
