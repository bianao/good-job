import request from '../utils'
import { ResponseData, LoginResponseData, UserInfo, NavList } from '../types/loginType'

// 获取导航列表
export const get_menulist = (data?: any) => {
    return request.get<ResponseData<NavList>>({
        url: `/api/sys/menu/nav`,
        data
    })
}

// 获取用户信息
export const get_userinfo = (data?: any) => {
    return request.get<ResponseData<UserInfo>>({
        url: `/api/sys/user/info`,
        data
    })
}
// 登录
export const _login = (params: any) => {
    return request.post<ResponseData<LoginResponseData>>({
        url: `/api/login?grant_type=admin`,
        data: params
    })
}


// 改密码
export const edit_password = (params: any) => {
    return request.post({
        url: `/api/sys/user/password`,
        data: params
    })
}






