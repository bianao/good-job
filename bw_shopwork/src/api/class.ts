import request from '../utils'
import { RequesClass, ProdClass } from '../types/product/prodClass'
import { PROUPLIST, RequesGroup } from '../types/product/group.d'


export const _get_produceC = (params: any) => { //分类管理
    //@ts-ignore
    return request.get<RequesClass<ProdClass>>({
        url: '/api/prod/category/table',
        method: 'GET',
        params: params
    })
}
export const _get_class = (params: any) => { //分类管理
    //@ts-ignore
    return request.get<RequesClass<ProdClass>>({
        url: '/api/prod/category/listCategory',
        method: 'GET',
        params: params
    })
}

export const _edit_produceC = (params: any) => { //分类管理
    //@ts-ignore
    return request.put<RequesClass<ProdClass>>({
        url: '/api/prod/category',
        method: 'PUT',
        data: params
    })
}
export const _add_produceC = (params: any) => { //分类管理
    //@ts-ignore
    return request.post({
        url: '/api/prod/category',
        method: 'POST',
        data: params
    })
}
export const _del_produceC = (params: any) => { //分类管理
    //@ts-ignore
    return request.delete({
        url: `/api/prod/category/${params}`,
        method: 'DELETE',
        data: [params]
    })
}
export const get_groupList = (params: any) => { //分组管理
    return request.get<RequesGroup<PROUPLIST>>({
        url: '/api/prod/prodTag/page',
        method: 'GET',
        params: params
    })
}
export const get_TaggroupList = (params: any) => { //分组管理
    return request.get<RequesGroup<PROUPLIST>>({
        url: '/api/prod/prodTag/listTagList',
        method: 'GET',
        params: params
    })
}
export const get_classList = (params: any) => { //分组管理
    return request.get<any>({
        url: `/api/prod/category/info/${params}`,
        method: 'GET',
        data: params
    } as any)
}

