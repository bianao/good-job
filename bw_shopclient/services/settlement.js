import request from "@/utils/http";

// 订单列表
export const getsetlist = (data) => request.post(`/p/order/confirm`, data)

//获取订单号
export const send_order = params => request.post(`/p/order/submit`, params)

// 确认支付
export const order_pay = params => request.post(`/p/order/pay`, params)