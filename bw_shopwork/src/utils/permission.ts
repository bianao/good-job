
import type { RouteRecordRaw } from 'vue-router';


export const getRoutes = (menyList: any) => {
    //最终返回的路由
    const routes: RouteRecordRaw[] = [];

    //所有的路由
    let allRoutes: RouteRecordRaw[] = [];

    // 获取router里面文件的路由
    const routeFiles = import.meta.globEager('@/router/module/*.ts')
    console.log(routeFiles);
    allRoutes = Object.keys(routeFiles).reduce((prev: any, next: string) => {
        prev.push(...routeFiles[next].default)
        return prev
    }, [])

    const changeRoutes = (menu: any) => {
        for (const item of menu) {
            // 判断type
            if (item.type === 1) {
                const route = allRoutes.find(val => val.path === '/' + item.url);
                if (route) routes.push(route)
            } else {
                changeRoutes(item.list)
            }
        }
    }
    changeRoutes(menyList)

    return routes;
}