import { createApp } from 'vue';
import './style.css';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import App from './App.vue';
import router from './router';
import { createPinia } from 'pinia'
import Avue from '@smallwei/avue';
import '../node_modules/@smallwei/avue/lib/index.css';
import locale from 'element-plus/lib/locale/lang/zh-cn'
import useStore from './store'; 

const app = createApp(App);
const pinia = createPinia();
app.use(pinia);
const {login} = useStore()

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
login.getMenu();
app.use(router).use(Avue).use(ElementPlus, { locale }).mount('#app')