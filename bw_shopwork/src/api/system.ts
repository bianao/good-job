import request from '../utils'
// 管理员列表
export const get_userlist = (params: any) => {
    return request.get({
        url: '/api/sys/user/page',
        params: params
    })
}
// 删除管理员
export const del_user = (params: any) => {
    return request.delete({
        url: '/api/sys/user',
        data: params
    })
}
// 新增管理员
export const add_user = (params: any) => {
    return request.post({
        url: '/api/sys/user',
        data: params
    })
}
// 编辑管理员
export const edit_user = (params: any) => {
    return request.put({
        url: '/api/sys/user',
        data: params
    })
}
// 角色列表
export const get_rolelist = (params: any) => {
    return request.get({
        url: '/api/sys/role/page',
        params
    })
}
// menutable列表
export const get_menutable = () => {
    return request.get({
        url: '/api/sys/menu/table',
    })
}
// 删除角色
export const del_role = (data: any) => {
    return request.delete({
        url: '/api/sys/role',
        data
    })
}
// 新增角色
export const add_role = (data: any) => {
    return request.post({
        url: '/api/sys/role',
        data
    })
}
// 编辑角色
export const edit_role = (data: any) => {
    return request.put({
        url: '/api/sys/role',
        data
    })
}
// 参数列表
export const get_canshulist = (params: any) => {
    return request.get({
        url: '/api/sys/config/page',
        params
    })
}
// 删除参数
export const del_canshu = (data: any) => {
    return request.delete({
        url: '/api/sys/config',
        data
    })
}
// 新增参数
export const add_canshu = (data: any) => {
    return request.post({
        url: '/api/sys/config',
        data
    })
}
// 编辑参数
export const edit_canshu = (data: any) => {
    return request.put({
        url: '/api/sys/config',
        data
    })
}
// 地址列表
export const get_arealist = () => {
    return request.get({
        url: '/api/admin/area/list',
        method: 'get',
    })
}
// 新增地址
export const add_area = (data: any) => {
    return request.post({
        url: '/api/admin/area',
        data
    })
}
// 编辑地址
export const edit_area = (data: any) => {
    return request.put({
        url: '/api/admin/area',
        data
    })
}
// 删除地址
export const del_area = (data: any) => {
    return request.delete({
        url: `/api/admin/area/${data}`,
        data
    })
}
// time列表
export const get_timelist = () => {
    return request.get({
        url: '/api/sys/schedule/page',
        method: 'get',
    })
}
// 菜单列表
export const get_menuTable = () => {
    return request.get({
        url: '/api/sys/menu/table',
    })
}
// 菜单级联列表
export const get_menulist = () => {
    return request.get({
        url: '/api/sys/menu/list',
    })
}

// 系统日志
interface addrtype {
    // @ts-ignore
    data: addrtype<addrtypedata>
    current: number;
    size: number;
}
interface addrtypedata {
    $cellEdit: boolean,
    $index: number,
    createDate: string,
    id: number,
    ip: string,
    method: string,
    operation: string,
    params: string,
    time: number,
    username: string
}
export const SystemList = (params: any) =>
    request.get<addrtype>({
        url: `/api/sys/log/page`,
        method: 'GET',
        params: params
    })

// 删除地址
export const del_menu = (data: any) => {
    return request.delete({
        url: `/api/sys/menu/${data}`,
        data
    })
}

// 新增地址
export const add_menu = (data: any) => {
    return request.post({
        url: '/api/sys/menu',
        data
    })
}
// 编辑地址
export const edit_menu = (data: any) => {
    return request.put({
        url: '/api/sys/menu',
        data
    })
}
// 获取角色编辑信息
export const getedit_data = (data: any) => {
    return request.get({
        url: `/api/sys/role/info/${data}`,
        data
    })
}

// 获取地址编辑信息
export const get_editlist = (data: any) => {
    return request.get({
        url: `/api/admin/area/info/${data}`,
        data
    })
}

