export const tableOption = {
    border: true,
    index: false,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    menuWidth: 350,
    align: 'center',
    refreshBtn: true,
    searchMenuSpan: 4,
    addBtn: false,
    editBtn: false,
    delBtn: false,
    viewBtn: false,
    rowKey: 'categoryId',
    rowParentKey: 'parentId',
    props: {
        label: 'label',
        value: 'value'
    },
    column: [{
        label: '分类名称',
        prop: 'categoryName',
        width: 180,
    }, {
        label: '图片',
        prop: 'pic',
        type: 'upload',
        width: 250,
        listType: 'picture-img'

    }, {
        width: 150,
        label: '状态',
        prop: 'status',
        slot: true,
        type: 'select',
        dicData: [
            {
                label: '未上架',
                value: 0
            }, {
                label: '上架',
                value: 1
            }
        ]
    },
    {
        label: '排序号',
        prop: 'seq'
    },
    ]
}as any
