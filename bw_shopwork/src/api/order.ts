import request from '../utils/index'
export const orderlist = (data: any) =>{
    return request.get({
        url: '/api/order/order/page',
        method:'GET',
        params: {
            current: data.current,
            size: data.size
        }
    })
    
}
export const ordermark = (data: any) =>{
    return request.get({
        url: `/api/order/order/orderInfo/${data.numbers}`,
        method:'GET',
        params: {
            t: data.t
        }
    })
    
}

export const order_Search=(params:any)=>{
    return request.get({
        url: `/api/order/order/page`,
        method:'GET',
        params
    })
}