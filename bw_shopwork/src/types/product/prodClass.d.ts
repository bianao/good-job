export interface RequesClass{
    status:number,
    statusText:string,
    data:ProdClass[]
}

export interface ProdClass{
    attributeIds:any,
    brandIds:any,
    brands:any,
    categories:any,
    categoryId:number,
    categoryName:string,
    grade:number,
    icon:any,
    parentId:number,
    pic:string,
    prodProps:any,
    products:any,
    recTime:string,
    seq:number,
    shopId:number,
    status:number,
    updateTime:string
}