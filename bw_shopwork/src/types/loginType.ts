export interface ResponseData<T> {
    status: number,
    statusText: string,
    data: T
}

export interface LoginResponseData {
    access_token: string,
    token_type: string,
    refresh_token: string,
    authorities: any[],
    shopId: number,
    expires_in: number,
    userId: number
}

export interface UserInfo {
    createTime: string,
    email: string,
    mobile: string,
    roleIdList: number[],
    shopId: number,
    status: number,
    userId: number,
    username: string,
}

export interface AuthoritiesItem {
    authority: string
}
export interface NavListItem {
    menuId: number,
    name: string,
    orderNum: number,
    parentId: number,
    parentName: string,
    perms: string,
    type: number,
    url: string
}
export interface NavList {
    authorities:AuthoritiesItem[],
    menuList:NavListItem[],
}

