export const rulesConfig={
    username:[
        {required:true,message:'请输入用户名',trigger:'blur'},
        {min:2,max:10,message:'长度不对',trigger:'blur'}
    ],
    pwd:[{required:true,message:'请输入密码',trigger:'blur'}],
    code:[{required:true,message:'请输入验证码',trigger:'blur'}]
}