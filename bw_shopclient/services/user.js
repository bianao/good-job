import request from '../utils/http'

//获取token 
export const _login=data=>request.post("/login",data)
//设置用户信息 用户授权之后拿到用户信息  保存
export const save_user_info=data=>request.put("/p/user/setUserInfo",data)
//获取用户信息
export const getUser=(query)=>request.get('/p/user/userInfo',query)


//获取用户收藏商品列表
export const getcollList=()=>request.get('/p/user/collection/prods')
//查询用户收藏商品数量
export const getcollCount=()=>request.get('/p/user/collection/count')


//获取用户的所有地址信息
export const getaddressList=()=>request.get('/p/address/list')
//根据地址id，获取地址信息
export const getaddressId=(addrId)=>request.put(`/p/address/defaultAddr/${addrId}`)
//根据省市区的pid获取地址信息
export const getAddressdata =(query)=>request.get('/p/area/listByPid',query)


//根据订单状态获取订单列表信息，状态为0时获取所有订单
export const getTabarr =(query)=>request.get('/p/myOrder/myOrder',query)
//根据订单号取消订单
export const getOrderNumber = (orderNumber) => request.put(`/p/myOrder/cancel/${orderNumber}`)
//根据订单号删除订单
export const deleteTabarr=(orderNumber)=>request.delete(`/p/myOrder/${orderNumber}`)
//订单详情信息
export const getOrderDeatil = (query) => request.get(`/p/myOrder/orderDetail`,query)



//用户的发送验证码
export const getPhone=(query)=>request.post('/p/sms/send',query)



//根据地址id，获取地址信息
export const getCity=(addrId)=>request.get(`/p/address/addrInfo/${addrId}`)
// 修改用户地址
export const updateAddr=(data)=>request.put('/p/address/updateAddr',data)
//新增用户地址
export const addAddress=(query)=> request.post('/p/address/addAddr',query)
//根据地址id，删除用户地址
export const  deleteRess=(addrId)=> request.delete(`/p/address/deleteAddr/${addrId}`)