import useproductStore from './module/product'
import useloginStore from './module/login'
import usesystemStore from './module/system'
import usevip from './module/vip'
import usetimingStore from './module/timeing'
import usenoticeStore from './module/shop'
import useList from './module/list'
export default function useStore() {
    return {
        login: useloginStore(),
        product: useproductStore(),
        system: usesystemStore(),
        timing: usetimingStore(),
        vip: usevip(),
        shop:usenoticeStore(),
        orderstore:useList(),
    }
}