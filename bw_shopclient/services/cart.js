import request from "../utils/http";

//购物车
export const cartApi = () => request.post('/p/shopCart/info')

// 加加减减
export const cartchangeItem = params => request.post('/p/shopCart/changeItem', params)

// 总价
export const zongjia = (data) => request.post('/p/shopCart/totalPay', data)


// 删除
export const dellist = (data) => request.delete('/p/shopCart/deleteItem', data)


export const requestsettlement = (data) => request.post('/p/order/confirm', data)

export const requestlist = () => request.get('/p/address/list')
//提交订单
export const requestplaceorder = (data) => request.post('/p/order/submit', data)
//支付接口
export const requestpay = (data) => request.post('/p/order/pay', data)


export const requestcarcount = () => request.get('/p/shopCart/prodCount')