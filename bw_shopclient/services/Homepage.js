import request from '../utils/http'

export const getNotice = () => request.get('/shop/notice/topNoticeList') //通知
export const getNoticeDeatil = (id) => request.get(`/shop/notice/info/+${id}`) //通知详情

export const getNewDay = (params) => request.get('/prod/prodListByTagId', params) //每日上新
export const getShopHot = (params) => request.get('/prod/prodListByTagId', params) //商城热卖
export const getMorBaby = (params) => request.get('/prod/prodListByTagId', params) //更多宝贝

export const getNewproduct = (params) => request.get('/prod/lastedProdPage', params) //新品推荐
export const getDailyrushed = (params) => request.get('/prod/moreBuyProdList', params) //每日疯抢

