export interface RequesGroup<T> {
    statusText: string
    status: number
    request: T
    headers: T
    data: T
    config: T
}

export interface PROUPLIST {
    [x: string]: any;
    createTime?: string,
    deleteTime?: null,
    id?: number,
    isDefault?: number,
    prodCount?: number,
    seq?: number,
    shopId?: number,
    status?: number,
    style?: number,
    title?: string,
    updateTime?: string,
}