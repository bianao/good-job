import config from "./config"


const message = {
    toast(title, type = 'text') {
        if (title.length > 10) {
            console.console.error(("toast 长度超过10个字符,当前长度为" + title.length));
        }
        let icon = 'none'
        if (type) {
            switch (type) {
                case "text":
                    icon = 'none'
                    break;
                case "suc":
                    icon = 'success'
                    break;
                case "err":
                    icon = 'error'
                    break;
            }
        }
        uni.showToast({
            title,
            type,
            icon
        });
    },
    confirm(title, confirmColor) {
        return new Promise((resolve, reject) => {
            uni.showModal({
                title,
                cancelColor: '#ccc',
                confirmColor: confirmColor || config.modaColor,
                success(result) {
                    if (result.cancel) {
                        reject(result)
                    } else {
                        resolve(result)
                    }
                }
            });
        })
    }
}


export default message