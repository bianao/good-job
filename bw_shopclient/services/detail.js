import request from "@/utils/http";

// 根据商品id 获取该商品的详情数据
export const _get_detail = prodId => request.get('/prod/prodInfo', {
    prodId
})
// 添加购物车
export const addShopApi = (data) => request.post('/p/shopCart/changeItem', data)
//详情评论
export const requestPingLun = (data) => request.get("/prodComm/prodCommPageByProd", data)
//详情点赞
export const requestAddLike = (data) => request.post("/p/user/collection/addOrCancel", data)

export const isLikeApi = (data) => {
    return request.get('/p/user/collection/isCollection', data)
}