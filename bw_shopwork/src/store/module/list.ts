import {defineStore} from 'pinia'
import { order_Search } from '../../api/order'
import * as dayjs from 'dayjs'
const OrderStore:any=defineStore('order',{
    state(){
        return{
            data:[],
            input:'',
            region:null,
            dataValue:[]
        }
    },
    actions:{
        async orderSearch(){
            console.log(this)
            await order_Search({
                orderNumber:this.input,
                status:this.region,
                kstartTime:this.dataValue[0],
                endTime:this.dataValue[1]
            })
        }
    }
})
export default OrderStore