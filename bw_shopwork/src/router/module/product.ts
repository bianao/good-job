const prodList = [ //产品管理
    {//分组管理
        path: '/prod/prodTag',
        name: 'prod:prodTag',
        component: () => import('../../views/home/product/group/index.vue')
    },
    {//产品管理
        path: '/prod/prodList',
        name: 'prod:prodList',
        component: () => import('../../views/home/product/produce/index.vue')
    },
    {//分类管理
        path: '/prod/category',
        name: 'prod:category',
        component: () => import('../../views/home/product/classify/index.vue')
    },
    {//评论管理
        path: '/prod/prodComm',
        name: 'prod:prodComm',
        component: () => import('../../views/home/product/comment/index.vue')
    },
    {//规格管理
        path: '/prod/spec',
        name: 'prod:spec',
        component: () => import('../../views/home/product/sku/index.vue')
    },
    {//公告管理
        path: '/shop/notice',
        name: 'shop:notice',
        component: () => import('../../views/home/storeManagement/notice/index.vue')
    },
    {//热搜管理
        path: '/shop/hotSearch',
        name: 'shop:hotSearch',
        component: () => import('../../views/home/storeManagement/toSearch/index.vue')
    },
    {//轮播图管理
        path: '/admin/indexImg',
        name: 'admin:indexImg',
        component: () => import('../../views/home/storeManagement/swiper/index.vue')
    },
    {//运费末班
        path: '/shop/transport',
        name: 'shop:transport',
        component: () => import('../../views/home/storeManagement/feight/index.vue')
    },
    {//自提点管理
        path: '/shop/pickAddr',
        name: 'shop:pickAddr',
        component: () => import('../../views/home/storeManagement/pickUpPoints/index.vue')
    },
    {//会员管理
        path: '/user/user',
        name: 'user:user',
        component: () => import('../../views/home/vip/index.vue')
    },
    {//订单管理
        path: '/order/order',
        name: 'order:order',
        component: () => import('../../views/home/list/index.vue')
    },
    {//地址管理
        path: '/sys/area',
        name: 'sys:area',
        component: () => import('../../views/home/systemmanagement/address/index.vue')
    },
    {//管理员管理
        path: '/sys/user',
        name: 'sys:user',
        component: () => import('../../views/home/systemmanagement/administrator/index.vue')
    },
    {//角色管理
        path: '/sys/role',
        name: 'sys:role',
        component: () => import('../../views/home/systemmanagement/role/index.vue')
    },
    {//菜单管理
        path: '/sys/menu',
        name: 'sys:menu',
        component: () => import('../../views/home/systemmanagement/menu/index.vue')
    },
    {//定时任务
        path: '/sys/schedule',
        name: 'sys:schedule',
        component: () => import('../../views/home/systemmanagement/timing/index.vue')
    },
    {//参数管理
        path: '/sys/config',
        name: 'sys:config',
        component: () => import('../../views/home/systemmanagement/parameter/index.vue')
    },
    {//系统管理
        path: '/sys/log',
        name: 'sys:log',
        component: () => import('../../views/home/systemmanagement/system/index.vue')
    },
]
export default prodList