import { defineStore } from 'pinia'
import { get_groupList } from '../../api/group'
const productStore: any = defineStore('product', {
    state() {
        return {
            groupList: [],
            produceList: [],
            classList: [],
            commentList: [],
            skuList: [],
            skuListItem: {}
        }
    },
    actions: {

    }
})
export default productStore