import App from './App'
import goPage from "@/utils/utils"
// 封装云函数
async function newCloud() {
  const n_clound = new wx.cloud.Cloud({
    resourceEnv: 'min-gucof',
    traceUser: true
  })
  // 初始化
  await n_clound.init();
  return n_clound;
}
Vue.prototype.$cloud = newCloud()

// #ifndef VUE3
import Vue from 'vue'
Vue.prototype.$goPage = goPage
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
  createSSRApp
} from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif