import request from "@/utils/http";

export const _classifyLeft = () => request.get('/category/categoryInfo')
//分类右侧  /prod/pageProd?categoryId=85
export const _classifyRight = (id) => request.get(`/prod/pageProd/${id}`)
