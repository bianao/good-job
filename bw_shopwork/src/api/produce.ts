import request from '../utils'
import { RequesProd, ProdLIST, ProdTYPE } from '../types/product/produce'

export const _get_produce = (params: any) => { //产品管理
    //@ts-ignore
    return request.get<RequesProd<ProdTYPE<ProdLIST>>>({
        url: '/api/prod/prod/page',
        method: 'GET',
        params: params
    })
}

export const _del_produce = (params: any) => { //产品管理删除
    //@ts-ignore
    return request.delete({
        url: '/api/prod/prod',
        method: 'DELETE',
        data: [params]
    })
}
export const _add_produce = (params: any) => { //产品管理新增
    //@ts-ignore
    return request.post({
        url: '/api/prod/prod',
        method: 'POST',
        data: params
    })
}
export const _edit_produce = (params: any) => { //产品管理编辑
    //@ts-ignore
    return request.put({
        url: '/api/prod/prod',
        method: 'PUT',
        data: params
    })
}
export const _edit_produces = (params: any) => { //产品管理编辑数据
    //@ts-ignore
    return request.get({
        url: `/api/prod/prod/info/${params.prodId}`,
        method: 'GET',
        data: params
    })
}
export const _img_produces = (params: any) => { //产品管理编辑数据图片
    //@ts-ignore
    return request.post({
        url: `/api/admin/file/upload/element`,
        method: 'POST',
        data: params
    })
}
export const _edit_transport = (params: any) => { //运费详情
    //@ts-ignore
    return request.get({
        url: `/api/shop/transport/info/${params}`,
        method: 'GET',
    })
}
export const _edit_skuList = (params: any) => { //规格名
    //@ts-ignore
    return request.get({
        url: `/api/prod/spec/list`,
        method: 'GET',
    })
}
